package com.nastya.notebook.service;

import com.nastya.notebook.entity.HistoryRecord;
import com.nastya.notebook.entity.Note;
import com.nastya.notebook.repository.HistoryRepository;
import com.nastya.notebook.repository.NoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class NoteServiceImpl implements NoteService {

    private NoteRepository noteRepository;
    private HistoryRepository historyRepository;

    @Autowired
    public void setNoteRepository(NoteRepository repository) {
        this.noteRepository = repository;
    }

    @Autowired
    public void setHistoryRepository(HistoryRepository repository) {
        this.historyRepository = repository;
    }


    @Override
    public Note getNoteById(Integer id) {
        return noteRepository.findNoteById(id);
    }

    @Override
    public void saveNote(Note note) {
        noteRepository.save(note);
        HistoryRecord historyRecord = new HistoryRecord(note);
        historyRepository.save(historyRecord);
    }

    @Override
    public void updateNote(Integer id, String title, String text) {
        Note updated = noteRepository.findNoteById(id);
        updated.setTitle(title);
        updated.setText(text);
        updated.setLastChanges(new Date());
        noteRepository.save(updated);
        HistoryRecord historyRecord = new HistoryRecord(updated);
        historyRepository.save(historyRecord);
    }

    @Override
    public void deleteNote(Integer id) {
        noteRepository.deleteById(id);
    }

    @Override
    public List<Note> findAllNotes() {
        return noteRepository.findAll();
    }

    @Override
    public List<Note> findAllNotesByUsername(String username) {
        return noteRepository.findAllByUsername(username);
    }

    @Override
    public List<HistoryRecord> findAllHistoryRecords() {
        return historyRepository.findAll();
    }

}