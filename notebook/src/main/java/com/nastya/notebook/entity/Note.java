package com.nastya.notebook.entity;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "note", schema = "note", catalog = "")
public class Note {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonProperty("ID")
    private int id;
    @Column(name = "title")
    @JsonProperty("Title")
    private String title;
    @Column(name = "text")
    @JsonProperty("Text")
    private String text;
    @Column(name = "creation_date")
    @JsonProperty("Creation date")
    private Date creationDate;
    @Column(name = "last_changes")
    @JsonProperty("Last changes")
    private Date lastChanges;
    @Column(name = "username")
    @JsonProperty("Username")
    private String username;

    public Note() {
    }

    public Note(String title, String text, String username) {
        this.title = title;
        this.text = text;
        this.username = username;
        this.creationDate = new Date();
        this.lastChanges = new Date();
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return this.title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getLastChanges() {
        return lastChanges;
    }

    public void setLastChanges(Date lastChanges) {
        this.lastChanges = lastChanges;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

}