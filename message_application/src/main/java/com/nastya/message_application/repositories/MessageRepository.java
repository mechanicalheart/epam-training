package com.nastya.message_application.repositories;

import com.nastya.message_application.model.Message;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;

@CrossOrigin
@Repository
public interface MessageRepository extends PagingAndSortingRepository<Message, Long>, QuerydslPredicateExecutor<Message> {

    List<Message> findBySenderNumber(String senderNumber);

    List<Message> findByRecipientNumber(String recipientNumber);

    List<Message> findBySenderNumberAndRecipientNumber(String senderNumber, String recipientNumber);
}
