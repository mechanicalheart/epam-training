package com.nastya.notebook.service;

import com.nastya.notebook.entity.HistoryRecord;
import com.nastya.notebook.entity.Note;

import java.util.List;

public interface NoteService {
    Note getNoteById(Integer id);
    void saveNote(Note note);
    void updateNote(Integer id, String title, String text);
    void deleteNote(Integer id);
    List<Note> findAllNotes();
    List<Note> findAllNotesByUsername(String username);
    List<HistoryRecord> findAllHistoryRecords();

}
