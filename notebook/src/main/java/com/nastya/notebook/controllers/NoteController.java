package com.nastya.notebook.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nastya.notebook.entity.HistoryRecord;
import com.nastya.notebook.entity.Note;
import com.nastya.notebook.service.NoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.security.Principal;
import java.util.List;

@Controller
public class NoteController {
    private NoteService service;

    @Autowired
    public NoteController(NoteService service) {
        this.service = service;
    }

    @Autowired
    public MessageSource messageSource;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String home(Model model, Authentication authentication, Principal principal, HttpServletRequest request) {
        if (authentication == null) {
            return "forward:/login";
        } else {
            model.addAttribute("user", principal.getName());
            List<Note> notebook = filterAndSort(request);
            model.addAttribute("notes", notebook);
            return "index";
        }
    }

    private List<Note> filterAndSort(HttpServletRequest request) {
        List<Note> notebook;
        if (request.isUserInRole("ADMIN")) {
            notebook = service.findAllNotes();
        } else {
            Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            String username = "";
            if (principal instanceof UserDetails) {
                username = ((UserDetails) principal).getUsername();
            } else {
                username = principal.toString();
            }
            notebook = service.findAllNotesByUsername(username);
        }
        return notebook;
    }

    @GetMapping("/new")
    public String newNote() {
        return "new";
    }

    @PostMapping("/save")
    public String saveNote(@RequestParam String text,
                           @RequestParam String title) {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username = "";
        if (principal instanceof UserDetails) {
            username = ((UserDetails) principal).getUsername();
        } else {
            username = principal.toString();
        }
        service.saveNote(new Note(title, text, username));
        return "redirect:/";
    }

    @GetMapping("/edit/{id}")
    public String edit(@PathVariable Integer id, Model model) {
        Note note = service.getNoteById(id);
        model.addAttribute("note", note);
        return "edit";
    }

    @PostMapping("/update")
    public String updateNote(@RequestParam Integer id,
                             @RequestParam String text,
                             @RequestParam String title) {
        service.updateNote(id, title, text);
        return "redirect:/";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable Integer id) {
        service.deleteNote(id);
        return "redirect:/";
    }

    @GetMapping("/export_json/{id}")
    public String export_json(@PathVariable Integer id) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        Note note = service.getNoteById(id);
        mapper.writeValue(new File("note" + note.getId() + ".json"), note);
        return "redirect:/";
    }

    @GetMapping("/import_json")
    public String import_json() {
        FileDialog dialog = new FileDialog((Frame) null, "Select File to Open");
        dialog.setMode(FileDialog.LOAD);
        dialog.setVisible(true);
        dialog.setAlwaysOnTop(true);
        String file = dialog.getFile();
        ObjectMapper mapper = new ObjectMapper();
        Note note;
        if (file != null) {
            try {
                note = mapper.readValue(new File(file), Note.class);
                service.saveNote(note);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return "redirect:/";
    }

    @GetMapping("/login")
    public String login() {
        return "/login";
    }

    @GetMapping("/403")
    public String error403() {
        return "/error/403";
    }

    @GetMapping("/history")
    public String history(Model model, Authentication authentication, Principal principal, HttpServletRequest request) {
        model.addAttribute("user", principal.getName());
        List<HistoryRecord> historyRecords = service.findAllHistoryRecords();
        model.addAttribute("history_records", historyRecords);
        return "history";
    }
}
