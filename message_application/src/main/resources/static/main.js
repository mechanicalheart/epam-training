var app = angular.module("MessagesManagement", []);

app.controller("MessageController", function ($scope, $http) {

    $scope.messages = [];

    _refreshMessageData();

    $scope.deleteMessage = function (message) {
        $http({
            method: 'DELETE',
            url: '/message/' + message.id
        }).then(_success, _error);
    };

    function _refreshMessageData() {
        $http({
            method: 'GET',
            url: '/messages'
        }).then(
            function (res) { // success
                $scope.messages = res.data;
            },
            function (res) { // error
                console.log("Error: " + res.status + " : " + res.data);
            }
        );
    }

    function _success(res) {
        _refreshMessageData();
    }

    function _error(res) {
        var data = res.data;
        var status = res.status;
        var header = res.header;
        var config = res.config;
        alert("Error: " + status + ":" + data);
    }
});