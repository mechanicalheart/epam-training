package com.nastya.message_application.controller;

import com.nastya.message_application.model.Message;
import com.nastya.message_application.repositories.MessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class MessageController {

    private final MessageRepository messageRepository;

    @Autowired
    public MessageController(MessageRepository messageRepository) {
        this.messageRepository = messageRepository;
    }

    @GetMapping("/messages")
    @ResponseBody
    public List<Message> getMessagesWithFilters(@RequestParam(required = false) String sender, @RequestParam(required = false) String recipient) {
        List<Message> messages;
        if (sender.equals("") && recipient.equals("")) {
            messages = (List<Message>) messageRepository.findAll();
        }
        else if (!sender.equals("") && !recipient.equals("")){
            messages = messageRepository.findBySenderNumberAndRecipientNumber(sender, recipient);
        }
        else if (!sender.equals("")) {
            messages = messageRepository.findBySenderNumber(sender);
        }
        else {
            messages = messageRepository.findByRecipientNumber(recipient);
        }
        return messages;
    }

    @GetMapping("/messages/{id}")
    @ResponseBody
    public Message findMessageById(@PathVariable("id") Message message) {
        return message;
    }

    @RequestMapping(value = "/messages/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public void deleteMessage(@PathVariable("id") long id) {

        System.out.println("(Service Side) Deleting message with Id: " + id);

        messageRepository.deleteById((long) id);
    }

}
