package com.nastya.message_application;

import com.nastya.message_application.model.Message;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.time.LocalDate;
import java.util.Random;

@SpringBootApplication
public class MessageApplication {

	public static void main(String[] args) {
		SpringApplication.run(MessageApplication.class, args);
	}

	@Bean
	CommandLineRunner init(com.nastya.message_application.repositories.MessageRepository messageRepository) {
		String[] numbers = new String[]{"(321) 205-4647", "(753) 769-6860","(303) 297-2247", "(369) 689-4542",
				"(278) 553-2956", "(721) 946-3387", "(867) 530-9828", "(532) 852-1587", "(423) 404-9573", "(669) 700-1595",
				"(406) 424-9301", "(210) 769-3526", "(269) 559-0073", "(393) 376-6472", "(599) 807-2125"};
		String[] texts = new String[]{"", "", "", "", "", "", ""};
		int[] errorCodes = new int[]{1, 2, 3};
		Random rand = new Random();
		return args -> {
			for (int i = 0; i < 45; i++) {
				Message message = new Message();
				message.setSenderNumber(numbers[rand.nextInt(15)]);
				message.setRecipientNumber(numbers[rand.nextInt(15)]);
				boolean sent = Math.random() < 0.5;
				message.setSent(sent);
				message.setText(texts[rand.nextInt(7)]);
				message.setSentDate(LocalDate.now());
				if (!sent) {
					message.setErrorCode(errorCodes[rand.nextInt(3)]);
					message.setNumberOfTries(1 + (int)(Math.random() * 10));
				}
				messageRepository.save(message);
			}
			messageRepository.findAll().forEach(System.out::println);
		};
	}

}
