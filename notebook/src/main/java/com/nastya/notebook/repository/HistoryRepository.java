package com.nastya.notebook.repository;

import com.nastya.notebook.entity.HistoryRecord;
import com.nastya.notebook.entity.Note;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface HistoryRepository extends JpaRepository<HistoryRecord, Integer> {
    List<HistoryRecord> findAll();

}
