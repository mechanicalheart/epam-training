package com.nastya.notebook.entity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "historyRecord", schema = "history", catalog = "")
public class HistoryRecord {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "note_id")
    private int noteId;
    @Column(name = "change_date")
    private Date changeDate;
    @Column(name = "title")
    private String title;
    @Column(name = "text")
    private String text;

    public HistoryRecord() {
    }

    public HistoryRecord(int noteId, String title, String text) {
        this.noteId = noteId;
        this.title = title;
        this.text = text;
        this.changeDate = new Date();
    }

    public HistoryRecord(Note note) {
        this.noteId = note.getId();
        this.title = note.getTitle();
        this.text = note.getText();
        this.changeDate = new Date();
    }

    public int getNoteId() {
        return noteId;
    }

    public void setNoteId(int noteId) {
        this.noteId = noteId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getChangeDate() {
        return changeDate;
    }

    public void setChangeDate(Date changeDate) {
        this.changeDate = changeDate;
    }

}
