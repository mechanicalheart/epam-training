package com.nastya.notebook.repository;

import com.nastya.notebook.entity.Note;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface NoteRepository extends JpaRepository<Note, Integer> {
    Note findNoteById(Integer id);
    void deleteById(Integer id);
    List<Note> findAll();
    List<Note> findAllByUsername(String username);

}
